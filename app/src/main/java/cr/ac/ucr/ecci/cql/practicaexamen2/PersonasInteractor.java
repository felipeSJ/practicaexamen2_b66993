package cr.ac.ucr.ecci.cql.practicaexamen2;

import java.util.List;

public interface PersonasInteractor{

    interface OnFinishedListener {
        void onFinished(List<Persona> items);
    }

    void getItems(OnFinishedListener listener);
}
