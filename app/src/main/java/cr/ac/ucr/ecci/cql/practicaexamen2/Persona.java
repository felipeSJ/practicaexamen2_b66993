package cr.ac.ucr.ecci.cql.practicaexamen2;

import android.os.Parcel;
import android.os.Parcelable;

public class Persona implements Parcelable {
    private String identificacion;
    private String nombre;
/*    private int edad;
    private String celular;
    private String direccion;
    private String email;*/

    public Persona(String identificacion, String nombre/*, int edad, String celular, String direccion, String email*/) {
        this.setIdentificacion(identificacion);
        this.setNombre(nombre);
/*        this.setEdad(edad);
        this.setCelular(celular);
        this.setDireccion(direccion);
        this.setEmail(email);*/
    }

    protected Persona(Parcel in) {
        identificacion = in.readString();
        nombre = in.readString();
/*        edad = in.readInt();
        celular = in.readString();
        direccion = in.readString();
        email = in.readString();*/
    }

    public static final Creator<Persona> CREATOR = new Creator<Persona>() {
        @Override
        public Persona createFromParcel(Parcel in) {
            return new Persona(in);
        }

        @Override
        public Persona[] newArray(int size) {
            return new Persona[size];
        }
    };

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

/*    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }*/

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(getIdentificacion());
        parcel.writeString(getNombre());
/*        parcel.writeInt(getEdad());
        parcel.writeString(getCelular());
        parcel.writeString(getDireccion());
        parcel.writeString(getEmail());*/
    }
}

