package cr.ac.ucr.ecci.cql.practicaexamen2;

public class CantRetrieveItemsException extends Exception{
    public CantRetrieveItemsException(String msg) {
        super(msg);
    }
}
