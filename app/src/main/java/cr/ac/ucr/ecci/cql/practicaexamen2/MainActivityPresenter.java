package cr.ac.ucr.ecci.cql.practicaexamen2;

public interface MainActivityPresenter {
    // resumir
    void onResume();
    // evento cuando se hace clic en la lista de elementos
    void onItemClicked(Persona item);
    // destruir
    void onDestroy();
}
