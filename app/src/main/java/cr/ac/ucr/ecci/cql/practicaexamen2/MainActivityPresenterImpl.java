package cr.ac.ucr.ecci.cql.practicaexamen2;

import java.util.List;

public class MainActivityPresenterImpl implements MainActivityPresenter, PersonasInteractor.OnFinishedListener{

    private MainActivityView mMainActivityView;
    private PersonasInteractor personasInteractor;

    public MainActivityPresenterImpl(MainActivityView mainActivityView){
        this.mMainActivityView = mainActivityView;
        // Capa de negocios (Interactor)
        this.personasInteractor = new PersonasInteractorImpl();
        //this.personasInteractor.getItems(this);
    }

    @Override
    public void onResume(){
        if(mMainActivityView!=null){
            mMainActivityView.showProgress();
        }
        // Obtener los items de la capa de negocios (Interactor)
        personasInteractor.getItems(this);
    }

    @Override
    public void onItemClicked(Persona item) {

    }

    // Evento de clic en la lista
    /*@Override
    public void onItemClicked(Persona item){
        if(mMainActivityView!=null){
            mMainActivityView.setFragment(item);
        }
    }*/

    @Override
    public void onDestroy(){
        mMainActivityView=null;
    }

    @Override
    public void onFinished(List<Persona> items){
        if(mMainActivityView!=null){
            mMainActivityView.setItems(items);
            mMainActivityView.hideProgress();
        }
    }
    // Retornar la vista
    public MainActivityView getMainView(){
        return mMainActivityView;
    }
}
