package cr.ac.ucr.ecci.cql.practicaexamen2;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;

public class IServiceDataSourceImpl implements IServiceDataSource{
    private static final String URL_REST = "https://bitbucket.org/lyonv/ci0161_i2020_exii/raw/4250ce77dc7fdb5f5c1324d8c8d5bf5ff3959740/Personas14.json";
    public static final String TAG_IMG = "IMG";

    @Override
    public List<Persona> obtainItems() throws CantRetrieveItemsException {
        List<Persona> items = null;
        // Se realizan las llamadas a las fuentes de datos de donde se obtienen los datos
        try {
            TaskServicioREST task = new TaskServicioREST();
            task.execute(URL_REST);
            String data = getArrayFromJson(task.get());
            Gson gson = new Gson();
            Persona[] jsonObject = gson.fromJson(data, Persona[].class);
            items = new ArrayList<>(Arrays.asList(jsonObject));
        } catch (Exception e) {
            throw new CantRetrieveItemsException(e.getMessage());
        }

        return items;
    }

    public String getArrayFromJson(String json){
        int start = json.indexOf("[");
        int end = json.length() - 1;
        return json.substring(start, end);
    }



    // Clase para la tarea asincronica en Servicio REST
    public class TaskServicioREST extends AsyncTask<String, Void, String> {

        public TaskServicioREST(){}

        @Override
        protected String doInBackground(String... urls) {
            // tomanos el parámetro del execute() y bajamos el contenido
            return loadContentFromNetwork(urls[0]);
        }

        // El resultado de la tarea tiene el archivo gson el cual mostramos
/*        public void onPostExecute(String result) {
            dataRest = result;
        }*/

        // metodo para bajar el contenido
        private String loadContentFromNetwork(String url) {
            try {
                InputStream mInputStream = (InputStream) new URL(url).getContent();
                InputStreamReader mInputStreamReader = new InputStreamReader(mInputStream);
                BufferedReader responseBuffer = new BufferedReader(mInputStreamReader);
                StringBuilder strBuilder = new StringBuilder(); String line = null;
                while ((line = responseBuffer.readLine()) != null) {
                    strBuilder.append(line);
                }
                return strBuilder.toString();
            } catch (Exception e) {
                Log.v(TAG_IMG, e.getMessage());
            }
            return null;
        }
    }
}