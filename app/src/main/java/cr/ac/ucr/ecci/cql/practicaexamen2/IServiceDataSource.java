package cr.ac.ucr.ecci.cql.practicaexamen2;

import java.util.List;

public interface IServiceDataSource {
    List<Persona> obtainItems() throws CantRetrieveItemsException;
}
