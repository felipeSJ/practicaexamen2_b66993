package cr.ac.ucr.ecci.cql.practicaexamen2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.FragmentTransaction;
import android.os.Bundle;

public class DetailActivity extends AppCompatActivity {

    private Persona persona;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        persona = getIntent().getParcelableExtra("persona");
        setFragment(persona);
    }

    public void setFragment(Persona item) {
        Bundle bundle = new Bundle();
        PersonaFragment details = new PersonaFragment();
        bundle.putParcelable("currentItem", item);
        details.setArguments(bundle);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.persona_details, details);
        ft.commit();
    }
}