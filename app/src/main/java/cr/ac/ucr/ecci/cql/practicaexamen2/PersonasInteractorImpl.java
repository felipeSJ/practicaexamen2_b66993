package cr.ac.ucr.ecci.cql.practicaexamen2;

import java.util.List;
import android.os.Handler;

public class PersonasInteractorImpl implements PersonasInteractor {
    private IServiceDataSource iServiceDataSource;

    @Override public void getItems(final OnFinishedListener listener) {
        // Enviamos el hilo de ejecucion con un delay para mostar la barra de progreso
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                List<Persona> items = null;
                iServiceDataSource = new IServiceDataSourceImpl();
                try {
                    // obtenemos los items
                    items = iServiceDataSource.obtainItems();
                } catch (CantRetrieveItemsException e) {
                    e.printStackTrace();
                }
                // Al finalizar retornamos los items
                listener.onFinished(items);
            }
        }, 3000);
    }
}
